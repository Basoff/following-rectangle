import cv2
import numpy as np

cv2.namedWindow("Image", cv2.WINDOW_AUTOSIZE)
cam = cv2.VideoCapture(0)
cam.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1)
cam.set(cv2.CAP_PROP_EXPOSURE, -7)

_, frame = cam.read()
r = cv2.selectROI(frame, fromCenter=False)
single = frame[int(r[1]):int(r[1]+r[3]), int(r[0]):int(r[0]+r[2])]

while cam.isOpened:
    _, frame = cam.read()

    many = frame

    orb = cv2.SIFT_create() #Работает медленно, но главное, что работает, в отличие от орбы (:))

    key_points1, descriptors1 = orb.detectAndCompute(single, None)
    key_points2, descriptors2 = orb.detectAndCompute(many, None)

    matcher = cv2.BFMatcher()
    matches = matcher.knnMatch(descriptors1, descriptors2, k=2)

    best = []

    for m1, m2 in matches:
        if m1.distance < 0.75 * m2.distance:
            best.append([m1])

    #print(f"All matches {len(matches)}, best matches {len(best)}")

    if len(best) > 5:
        src_pts = np.float32([key_points1[m[0].queryIdx].pt for m in best]).reshape(-1, 1, 2)
        dst_pts = np.float32([key_points2[m[0].trainIdx].pt for m in best]).reshape(-1, 1, 2)
        M, hmask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0) #Работает очень сложно, можно не вникать. Главное, что работает.
        h, w, _ = single.shape
        pts = np.float32([[0, 0], [0, h-1], [w-1, h-1], [w-1, 0]]).reshape(-1, 1, 2)
        dst = cv2.perspectiveTransform(pts, M)
        cv2.polylines(frame, [np.int32(dst)], True, 255, 3, cv2.LINE_AA)
    else:
        print("Not enough matches")
        mask = None

    matches_image = cv2.drawMatchesKnn(single, key_points1, 
                                many, key_points2,
                                best, None)

    cv2.imshow("Image", frame)

    key = cv2.waitKey(10)
    if key > 0:
        if chr(key) == "d":
            break
cv2.destroyAllWindows()